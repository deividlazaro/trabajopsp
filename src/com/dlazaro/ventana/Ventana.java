package com.dlazaro.ventana;

import com.dlazaro.ventana.GUI.Panel;
import com.dlazaro.ventana.GUI.Splash;
import com.dlazaro.ventana.Tarea.descarga;
import com.sun.javaws.ui.SplashScreen;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.*;
import java.util.ArrayList;
import static com.dlazaro.ventana.Util.Constantes.PATH;
import static com.dlazaro.ventana.Util.Constantes.PATHCONFIG;
/**
 * Created by DAM on 26/11/2015.
 */
public class Ventana implements ActionListener {
    private JPanel panel1;
    private JTextField tfURL;
    private JButton btanadirpanel;
    private JButton btiniciarD;
    private JPanel panel2;
    private JCheckBox chkbDescarga;
    private JList list1;
    private JButton btcambiar;
    private JTextField tflimite;
    private ArrayList<Panel> listaPanel;
    private JFileChooser choo;
    private DefaultListModel modeloLista;
    private String rutaConfig;

    private File ruta2;

    public Ventana(){

        Thread splash1=new Thread(new Runnable() {
            @Override
            public void run() {
                Splash splash=new Splash();

                try {

                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                }
                splash.setVisible(false);
            }
        });
        splash1.start();


        btanadirpanel.addActionListener(this);
        btiniciarD.addActionListener(this);
        JFrame frame = new JFrame("Ventana");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);


        modeloLista=new DefaultListModel();
        list1.setModel(modeloLista);
        File fichero=new File(PATH);
        File ficheroConf=new File(PATHCONFIG);
        if(fichero.exists()){
            cargarFichero();
        }
        if(ficheroConf.exists()){
            try {
                leerRuta();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else{
            try {
                elegirRuta();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        listaPanel=new ArrayList<>();
    }

    private void leerRuta() throws IOException {
        BufferedReader reader=new BufferedReader(new FileReader(PATHCONFIG));
        rutaConfig=reader.readLine();
        reader.close();
    }

    private void elegirRuta() throws IOException {
        choo=new JFileChooser();
        choo.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        if(choo.showSaveDialog(null)==JFileChooser.CANCEL_OPTION){

            JOptionPane.showMessageDialog(null,"No has seleccionado ruta , seria mejor que lo hicieras", "!Eh tu�",JOptionPane.ERROR_MESSAGE);
            return;
        }
        rutaConfig=choo.getSelectedFile().getAbsolutePath();
        PrintWriter writer=new PrintWriter(new BufferedWriter(new FileWriter(PATHCONFIG,false)));
        writer.println(rutaConfig);
        writer.close();

    }

    public  void eliminarPanel(Panel p){
        listaPanel.remove(p);
        panel2.remove(p.panelD);
        panel2.repaint();
        panel2.revalidate();
        panel2.updateUI();

    }

    public void setModeloLista(DefaultListModel modeloLista) {
        this.modeloLista = modeloLista;
    }

    public DefaultListModel getModeloLista() {
        return modeloLista;
    }

    public void cargarFichero(){

            try {
                BufferedReader reader=new BufferedReader(new FileReader(PATH));
                String linea=reader.readLine();
                while (linea!=null){
                    modeloLista.addElement(linea);
                    linea=reader.readLine();
                }
                reader.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    public void guardarFichero(Panel panel) throws IOException {
        PrintWriter writer=new PrintWriter(new BufferedWriter(new FileWriter(PATH,true)));
        writer.println(panel.toString());
        writer.close();
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        String actioncommand=((JButton) actionEvent.getSource()).getActionCommand();
        Panel panel=null;

        switch (actioncommand){

            case "anadir":
               String[] nombreF=tfURL.getText().split("/");
                if(listaPanel.size()<Integer.parseInt(tflimite.getText())) {
                    if(chkbDescarga.isSelected()){
                        try {
                            elegirRuta();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    panel = new Panel(tfURL.getText(),this,rutaConfig,nombreF[nombreF.length-1]);
                    panel2.add(panel.panelD);
                    panel2.revalidate();
                    listaPanel.add(panel);
                }
                else{
                    JOptionPane.showMessageDialog(null,"Demasiadas descargas ", "!Ups�",JOptionPane.ERROR_MESSAGE);
                }
                break;
            case "iniciar":
                    for(Panel p:listaPanel){
                        p.descargarFich();
                    }
                break;
            case "cambiar":
                try {
                    elegirRuta();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
       }
    }
}
