package com.dlazaro.ventana.GUI;

import com.dlazaro.ventana.Tarea.descarga;

import com.dlazaro.ventana.Ventana;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

/**
 * Created by DAM on 26/11/2015.
 */
public class Panel implements ActionListener{
    private JProgressBar pbdescarga;
    public JPanel panelD;
    private JButton btpararDescarga;
    private JButton btquitarpanel;
    private JLabel tamanototal;
    private String tfURL;
    private descarga descarga;
    private Ventana ventana;
    private String ruta;
    private String nombre;
    private tiposDescarga finale=null;

    public enum tiposDescarga{
        DESCARGADA,
        CANCELADA;
    }


public Panel(String url,Ventana ventana,String selectedFile,String nombre){
    this.tfURL=url;
    this.ventana=ventana;
    this.nombre=nombre;
    btpararDescarga.addActionListener(this);
    btquitarpanel.addActionListener(this);
    this.ruta=selectedFile;

}





   public void descargarFich(){
        try{
            descarga=new descarga(tfURL,ruta,this);
            descarga.addPropertyChangeListener(new PropertyChangeListener() {
                @Override
                public void propertyChange(PropertyChangeEvent event) {
                    if (event.getPropertyName().equals("progress")) {
                        pbdescarga.setValue((Integer) event.getNewValue());
                    }
                    if(event.getPropertyName().equals("bytes")){
                        tamanototal.setText(String.valueOf(event.getNewValue())+" bytes");
                    }

                }
            });
            descarga.execute();
        }catch (Exception e) {
            if (e instanceof MalformedURLException)
                JOptionPane.showMessageDialog(null, "La URL no es correcta", "Descargar Fichero", JOptionPane.ERROR_MESSAGE);
            else if (e instanceof FileNotFoundException)
                JOptionPane.showMessageDialog(null, "No se ha podido leer el fichero origen", "Descargar Fichero", JOptionPane.ERROR_MESSAGE);
            else
                JOptionPane.showMessageDialog(null, "No se ha podido leer el fichero origen", "Descargar Fichero", JOptionPane.ERROR_MESSAGE);

            e.printStackTrace();
        }
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        switch (actionEvent.getActionCommand()){
            case "parar":
                if(finale==null) {
                    descarga.cancel(true);
                    finale = tiposDescarga.CANCELADA;
                    anadiraModelo();
                    guardar();
                }


                break;
            case "borrar":
                ventana.eliminarPanel(this);

                if(finale==null) {
                    finale = tiposDescarga.CANCELADA;
                    anadiraModelo();
                    guardar();
                }
                descarga.cancel(true);
                break;
        }
    }


    public  void anadiraModelo() {
        ventana.getModeloLista().addElement(nombre+String.valueOf(finale));
    }

    public void guardar(){
        try {
            ventana.guardarFichero(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public tiposDescarga getFinale() {
        return finale;
    }

    public void setFinale(tiposDescarga finale) {
        this.finale = finale;
    }

    public String toString(){
        return nombre +": "+ String.valueOf(finale);


    }
}

