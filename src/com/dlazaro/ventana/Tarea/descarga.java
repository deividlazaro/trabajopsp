package com.dlazaro.ventana.Tarea;

import com.dlazaro.ventana.GUI.Panel;


import javax.swing.*;
import javax.swing.tree.ExpandVetoException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by DAM on 26/11/2015.
 */
public class descarga extends SwingWorker<Void,Integer> {
    private String urlFichero;
    private String rutaFichero;
    private int tamanofichero;
    private int progresoD;
    private Panel panel;
    public int getProgresoD() {
        return progresoD;
    }




    public descarga(String urlFichero,String rutaFichero,Panel panel){
        this.urlFichero=urlFichero;
        this.rutaFichero=rutaFichero;
        this.panel=panel;

    }




    @Override
    protected Void doInBackground()throws Exception{
       URL url= new URL(urlFichero);
        URLConnection conexion =url.openConnection();
        tamanofichero=conexion.getContentLength();
        String nombreFich[]=urlFichero.split("/");
        InputStream is=url.openStream();
        FileOutputStream fos=new FileOutputStream(rutaFichero+"\\"+nombreFich[nombreFich.length-1]);
        byte[] bytes=new byte[2048];
        int longitud=0;
         progresoD=0;

        while((longitud=is.read(bytes))!= -1){
            fos.write(bytes,0,longitud);
            progresoD += longitud;
            firePropertyChange("bytes",null,progresoD);
            setProgress((int) (progresoD * 100 / tamanofichero));
            firePropertyChange("tamano", null, tamanofichero);

            Thread.sleep(500);
            if(isCancelled())
                break;



        }

        is.close();
        fos.close();
        setProgress(100);
        panel.setFinale(Panel.tiposDescarga.DESCARGADA);
        panel.anadiraModelo();
        panel.guardar();


        return null;
    }
}
